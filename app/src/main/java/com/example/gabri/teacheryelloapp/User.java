package com.example.gabri.teacheryelloapp;

class User {

    // Variables
    private String id, name, email, password;

    // Empty Constructor Method.
    User(){}

    // Constructor Method with Parameters.
    User(String id, String name, String email, String password){
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
    }

    // Get ID.
    public String getId(){
        return id;
    }

    // Set ID
    public void setId(String id) {
        this.id = id;
    }

    // Get Name
    public String getName() {
        return name;
    }

    // Set Name
    public void setName(String name) {
        this.name = name;
    }

    // Get Email
    public String getEmail() {
        return email;
    }

    // Set Email
    void setEmail(String email) {
        this.email = email;
    }

    // Get Password
    String getPassword() {
        return password;
    }

    // Set Password
    void setPassword(String password) {
        this.password = password;
    }
}
