package com.example.gabri.teacheryelloapp;

public interface ISpeechRecognizer {
    void promptSpeechInput();
}
