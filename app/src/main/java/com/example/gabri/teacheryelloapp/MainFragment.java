package com.example.gabri.teacheryelloapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

// Main fragment class
public class MainFragment extends Fragment {

    // Variables
    View rootView;
    ListView listView;

    // Empty constructor method
    public MainFragment() {}

    // Create the view of the list
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_main, container, false);

        // Find list on the view
        listView = (ListView) rootView.findViewById(R.id.main_list);

        // Add values to the list
        String[] elements = new String[] {"Basic","Intermediate","Advanced"};

        // Create adapter for the list
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_expandable_list_item_1, elements);
        listView.setAdapter(adapter);

        // Assign click function to each item on the list
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position){
                    case 0:
                        // Goes to the first exercise
                        Intent vowelsIntent = new Intent(getContext(), PracticesActivity.class);
                        vowelsIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(vowelsIntent);
                        break;
                    case 1:
                        Toast.makeText(getContext(),R.string.level_disable,Toast.LENGTH_LONG).show();
                        break;
                    case 2:
                        Toast.makeText(getContext(),R.string.level_disable,Toast.LENGTH_LONG).show();
                        break;
                }
            }
        });
        return rootView;
    }
}
