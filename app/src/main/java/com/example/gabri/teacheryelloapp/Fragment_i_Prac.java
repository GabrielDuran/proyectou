package com.example.gabri.teacheryelloapp;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.Locale;
import static android.app.Activity.RESULT_OK;
import static com.facebook.FacebookSdk.getApplicationContext;

public class Fragment_i_Prac extends Fragment { // implements View.OnClickListener

    ListView iPracticeList;
    String speechResult;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_i_practice, container, false);

        iPracticeList = (ListView) rootView.findViewById(R.id.i_practice_list);

        Sentences sentences_data[] = new Sentences[]{
                new Sentences(R.drawable.ic_green_mic, "See you next week", ""),
                new Sentences(R.drawable.ic_green_mic, "See you at three", ""),
                new Sentences(R.drawable.ic_green_mic, "She received her teaching degree", ""),
                new Sentences(R.drawable.ic_green_mic, "Pleased to meet you", ""),
                new Sentences(R.drawable.ic_green_mic, "Steve eats cream cheese", ""),
        };

        SentencesAdapter adapter = new SentencesAdapter(getContext(), R.layout.listview_item_row, sentences_data);
        iPracticeList.setAdapter(adapter);

        return rootView;
    }

    public void promptSpeechInput(){
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.ENGLISH);
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Start to talk");

        try {
            startActivityForResult(intent,100);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(getApplicationContext(),R.string.speech_not_supported,Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        switch (requestCode) {
            case 100: if (resultCode == RESULT_OK && intent != null) {
                ArrayList<String> result = intent.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                setSpeechResult(result.get(0));
            }
                break;
        }
    }

    public void setSpeechResult(String result){
        this.speechResult = result;
    }

    public String getSpeechResult(){
        return this.speechResult;
    }
}
