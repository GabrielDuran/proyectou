package com.example.gabri.teacheryelloapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    // Variables
    private Button btnSignUp;
    private EditText etEmail, etName, etPassword, etConfirmPassword;
    private ProgressDialog progressDialog;
    private FirebaseAuth firebaseAuth;

    // Allow insert data into database
    private DatabaseReference databaseReference;

    // On Create method
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        // Match variables with U/I id elements
        etName = (EditText) findViewById(R.id.etName);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etPassword = (EditText) findViewById(R.id.etPassword);
        etConfirmPassword = (EditText) findViewById(R.id.etConfirmPassword);
        btnSignUp = (Button) findViewById(R.id.btnSignUp);

        // ActionBar
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Create a new progress bar
        progressDialog = new ProgressDialog(this);

        firebaseAuth = FirebaseAuth.getInstance();
        databaseReference = FirebaseDatabase.getInstance().getReference().child("Users");

        // Set on click method
        btnSignUp.setOnClickListener(this);
    }

    // Register user into database
    private void registerUser (){
        // Get text from edit text
        final String name = etName.getText().toString().trim();
        final String email = etEmail.getText().toString().trim();
        String password = etPassword.getText().toString().trim();
        String confirm_password = etConfirmPassword.getText().toString().trim();

        // Create an email pattern
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        // Validate that not must exist empty fields
        if (TextUtils.isEmpty(email) || TextUtils.isEmpty(name) || TextUtils.isEmpty(password) || TextUtils.isEmpty(confirm_password)) {
            // All data are required.
            Toast.makeText(this, R.string.data_require, Toast.LENGTH_SHORT).show();
        }
        // Validate password and confirm password must be equals
        else if (!password.equals(confirm_password)) {
            // Password and confirm password must be equals
            Toast.makeText(this, R.string.not_match, Toast.LENGTH_SHORT).show();
        }
        // Validate email address pattern
        else if (!email.matches(emailPattern)){
            Toast.makeText(this, R.string.invalid_email_address, Toast.LENGTH_SHORT).show();
        }
        // Register the user
        else {
            // Show progress dialog
            progressDialog.setMessage("Signing Up...");
            progressDialog.show();
            // Encrypts password text
            final String encrypt_password = md5(password);
            // Creates a new user
            setUpUser();
            // Creates a email and password authentication
            firebaseAuth.createUserWithEmailAndPassword(email, encrypt_password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                //User user = new User();
                                String user_id = task.getResult().getUser().getUid();
                                DatabaseReference current_user = databaseReference.child("user_id: "+user_id);
                                current_user.child("name: ").setValue(name);
                                current_user.child("email: ").setValue(email);
                                current_user.child("password: ").setValue(encrypt_password);
                                current_user.child("image: ").setValue("default");

                                // Start Login Activity
                                FirebaseAuth.getInstance().signOut();
                                Intent loginIntent = new Intent(RegisterActivity.this, LoginActivity.class);
                                loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(loginIntent);

                                // Close progress dialog
                                progressDialog.dismiss();

                                // User registered successfully
                                Toast.makeText(RegisterActivity.this, R.string.registered_successfully, Toast.LENGTH_SHORT).show();
                            } else {
                                // Close progress dialog
                                progressDialog.dismiss();
                                // Could not register. Please try again
                                Toast.makeText(RegisterActivity.this, R.string.register_faild, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }
    }

    // On activity stop
    @Override
    protected void onStop() {
        super.onStop();
    }

    // On back button pressed
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        // Goes to login activity
        goLoginActivity();
    }

    // On button sign up is clicked
    @Override
    public void onClick(View v) {
        if (v == btnSignUp) {
            // Register new user
            registerUser();
        }
    }

    // Sets up a new user by fetching the user entered details
    protected void setUpUser() {
        User user = new User();
        user.setName(etName.getText().toString());
        user.setEmail(etEmail.getText().toString());
        user.setPassword(md5(etPassword.getText().toString()));
    }

    // Goes login activity
    public void goLoginActivity(){
        Intent loginIntent = new Intent(this, LoginActivity.class);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(loginIntent);
    }

    // Encrypt password
    private static String md5 (final String password) {
        try {
            MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
            digest.update(password.getBytes());
            byte messageDigest[] = digest.digest();

            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest){
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e){
            e.printStackTrace();
        }
        return "";
    }
}