package com.example.gabri.teacheryelloapp;

public class Sentences {

    // Variables
    int button;
    String sentence, result;

    public Sentences(){super();}

    public Sentences(int button, String sentence, String result){
        this.button = button;
        this.sentence = sentence;
        this.result = result;
    }
}
