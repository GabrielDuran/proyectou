package com.example.gabri.teacheryelloapp;

import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;
import java.util.Locale;

public class Fragment_i_Exp  extends Fragment implements View.OnClickListener {

    // Variables
    Button btnHearPronunciation;
    TextToSpeech textToSpeech;
    int result;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_i_explanation, container, false);

        // Get button id
        btnHearPronunciation = (Button) rootView.findViewById(R.id.btnHearSound);

        textToSpeech = new TextToSpeech(getContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS){
                    result = textToSpeech.setLanguage(Locale.US);
                } else {
                    // Feature not supported in your device.
                    Toast.makeText(getContext(), R.string.feature_not_supported, Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnHearPronunciation.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (textToSpeech != null){
            textToSpeech.stop();
            textToSpeech.shutdown();
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnHearSound) {
            if (result == TextToSpeech.LANG_NOT_SUPPORTED || result == TextToSpeech.LANG_MISSING_DATA){
                Toast.makeText(getContext(), R.string.feature_not_supported, Toast.LENGTH_SHORT).show();
            } else {

                textToSpeech.setPitch(1.8f);
                String text = "<speak xml:lang=\"en-US\"> <phoneme alphabet=\"ipa\" ph=\"i:\"/>.</speak>";
                textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null, null);
            }
        }
    }
}
