package com.example.gabri.teacheryelloapp;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.speech.RecognizerIntent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;
import static com.facebook.FacebookSdk.getApplicationContext;

public class SentencesAdapter extends ArrayAdapter<Sentences>{
    private Context myContext;
    private int myLayoutResourceID;
    private Sentences myData[] = null;

    public SentencesAdapter(Context context, int layoutResourceID, Sentences[] data){
        super(context, layoutResourceID, data);
        this.myContext = context;
        this.myLayoutResourceID = layoutResourceID;
        this.myData = data;
    }

    public View getView(int position, View convertView, ViewGroup parent){
        View row = convertView;
        final SentencesHolder holder; // null;
        if (row == null){
            LayoutInflater inflater = ((Activity)myContext).getLayoutInflater();
            row = inflater.inflate(myLayoutResourceID, parent, false);
            holder = new SentencesHolder();
            holder.button = (ImageView) row.findViewById(R.id.image_mic);
            holder.sentence = (TextView) row.findViewById(R.id.sentence);
            holder.result = (TextView) row.findViewById(R.id.result);
            holder.button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Fragment_i_Prac speech = new Fragment_i_Prac();
                    speech.promptSpeechInput();
                }
            });
            row.setTag(holder);
        } else {
            holder = (SentencesHolder) row.getTag();
        }
        Sentences  sentences = myData[position];
        holder.button.setImageResource(sentences.button);
        holder.sentence.setText(sentences.sentence);
        holder.result.setText(sentences.result);
        return row;
    }

//    public void promptSpeechInput(){
//        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
//        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.ENGLISH);
//        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Start to talk");
//
//        try {
//            startActivityForResult(intent,100);
//        } catch (ActivityNotFoundException e) {
//            Toast.makeText(getApplicationContext(),R.string.speech_not_supported,Toast.LENGTH_LONG).show();
//        }
//    }

//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
//        super.onActivityResult(requestCode, resultCode, intent);
//
//        switch (requestCode) {
//            case 100: if (resultCode == RESULT_OK && intent != null) {
//                ArrayList<String> result = intent.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
//                setSpeechResult(result.get(0));
//            }
//                break;
//        }
//    }

    private static class SentencesHolder{
        ImageView button;
        TextView sentence;
        TextView result;
    }
}
