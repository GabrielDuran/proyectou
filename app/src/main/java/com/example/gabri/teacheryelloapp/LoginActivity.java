package com.example.gabri.teacheryelloapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

// Class login activity
public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    // Variables
    private EditText etEmail, etPassword;
    private Button btnSignIn;
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener authStateListener;
    private ProgressDialog progressDialog;
    private CallbackManager callbackManager;
    private DatabaseReference databaseReference;

    // On create activity
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Initialize firebase authentication function
        firebaseAuth = FirebaseAuth.getInstance();

        FirebaseUser user = firebaseAuth.getCurrentUser();

        // Validate if exist a session
        if (user != null) {
            // Start Login Activity
            Intent mainIntent = new Intent(LoginActivity.this, MainActivity.class);
            String uid = firebaseAuth.getCurrentUser().getUid();
//            String image = firebaseAuth.getCurrentUser().getPhotoUrl().toString();
            mainIntent.putExtra("user_id", uid);
//            if(image!=null || image!=""){
//                mainIntent.putExtra("profile_picture",image);
//            }
            startActivity(mainIntent);
        }

        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {}
        };

        // Match variables with U/I id elements
        etEmail = (EditText) findViewById(R.id.etEmail);
        etPassword = (EditText) findViewById(R.id.etPassword);
        btnSignIn = (Button) findViewById(R.id.btnSignIn);

        // Creates a new child in firebase database
        databaseReference = FirebaseDatabase.getInstance().getReference().child("Users");

        // Initialize Facebook Login button
        callbackManager = CallbackManager.Factory.create();
        LoginButton loginButton = (LoginButton) findViewById(R.id.facebook_login_button);
        // Set login button user permissions
        loginButton.setReadPermissions("email", "public_profile");
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {

            // When login is success
            @Override
            public void onSuccess(LoginResult loginResult) {
                signInWithFacebook(loginResult.getAccessToken());
            }

            // When login is canceled
            @Override
            public void onCancel() {
                // Login was canceled
                Toast.makeText(getApplicationContext(), R.string.login_canceled, Toast.LENGTH_SHORT).show();
            }

            // When exist an error while login
            @Override
            public void onError(FacebookException error) {
                if (error.toString().equals("net::ERR_NAME_NOT_RESOLVED")){
                    // You are offline, please connect and try again
                    Toast.makeText(getApplicationContext(), R.string.internet_connection_error, Toast.LENGTH_SHORT).show();
                } else {
                    // An error occurred while login, try again later
                    Toast.makeText(getApplicationContext(), R.string.error_when_login, Toast.LENGTH_SHORT).show();
                }
            }
        });

        // Initialize the progress dialog
        progressDialog = new ProgressDialog(this);

        // Set on click method to button sign in
        btnSignIn.setOnClickListener(this);
    }

    // On start activity
    @Override
    protected void onStart() {
        super.onStart();
        // Start firebase auth listener
        firebaseAuth.addAuthStateListener(authStateListener);
    }

    // On stop activity
    @Override
    public void onStop() {
        super.onStop();
        // Remove firebase auth listener
        if (authStateListener != null) {
            firebaseAuth.removeAuthStateListener(authStateListener);
        }
    }

    // Facebook login result
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    // Creates a new user
    protected void setUpUser() {
        User user = new User();
        user.setEmail(etEmail.getText().toString());
        user.setPassword(md5(etPassword.getText().toString()));
    }

    // When user login by facebook
    private void signInWithFacebook(AccessToken accessToken) {

        //Shows the progress dialog
        progressDialog.setMessage("Signing In...");
        progressDialog.show();

        // Creates a new credential to firebase facebook login
        AuthCredential credential = FacebookAuthProvider.getCredential(accessToken.getToken());
        firebaseAuth.signInWithCredential(credential).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                // If login is successful
                if (task.isSuccessful()){

                    // Get facebook user data.
                    String uid = task.getResult().getUser().getUid();
                    String name = task.getResult().getUser().getDisplayName();
                    String email = task.getResult().getUser().getEmail();
                    String image = task.getResult().getUser().getPhotoUrl().toString();

                    //Create a new user and save it in firebase database
                    DatabaseReference current_user = databaseReference.child("facebook_user_id: "+uid);
                    current_user.child("name: ").setValue(name);
                    current_user.child("email: ").setValue(email);
                    current_user.child("image: ").setValue(image);
                    goMainActivity();

                    // Close progress dialog
                    progressDialog.dismiss();

                }else {
                    // An error occurred while login, try again later
                    Toast.makeText(getApplicationContext(),R.string.error_when_login,Toast.LENGTH_LONG).show();

                    // Close progress dialog
                    progressDialog.dismiss();
                }
            }
        });
    }

    // User login
    private void signIn(String email, String password) {

        // Validate that not must exist empty fields
        if (TextUtils.isEmpty(email) || TextUtils.isEmpty(password)) {
            // All data are required.
            Toast.makeText(this, R.string.data_require, Toast.LENGTH_SHORT).show();
        }
        // Log the user in
        else {

            // Show the progress dialog
            progressDialog.setMessage("Signing In...");
            progressDialog.show();

            // Encrypts password text
            String encrypt_password = md5(password);

            // Assign data to user
            setUpUser();

            // Makes firebase authentication by email and password
            firebaseAuth.signInWithEmailAndPassword(email, encrypt_password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {

                            // If login is successful
                            if (task.isSuccessful()) {
                                // Goes to main activity
                                goMainActivity();

                                // Close progress dialog
                                progressDialog.dismiss();
                            } else {
                                // Email or password incorrect, please try again
                                Toast.makeText(LoginActivity.this, R.string.login_faild, Toast.LENGTH_SHORT).show();

                                // Close progress dialog
                                progressDialog.dismiss();
                            }
                        }
                    });
        }
    }

    // When user click on sign in button
    @Override
    public void onClick(View v) {
        if (v == btnSignIn) {
            // Get text field data
            signIn(etEmail.getText().toString().trim(), etPassword.getText().toString().trim());
        }
    }

    // Goes to Main Activity
    public void goMainActivity(){
        Intent mainIntent = new Intent(LoginActivity.this, MainActivity.class);
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(mainIntent);
        finish();
    }

    // Goes to Register Activity
    public void goRegisterActivity(View view) {
        Intent registerIntent = new Intent(this, RegisterActivity.class);
        registerIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(registerIntent);
    }

    // Encrypt password
    private static String md5 (final String password) {
        try {
            MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
            digest.update(password.getBytes());
            byte messageDigest[] = digest.digest();

            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest){
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e){
            e.printStackTrace();
        }
        return "";
    }
}
